require 'faker'

FactoryGirl.define do

  factory :task do 
    title         { Faker::Name.title }
    description   { Faker::Lorem.sentence }
    done            false
  end

end