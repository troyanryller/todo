require 'faker'

FactoryGirl.define do
#   factory :user do 
#     first_name              { Faker::Name.first_name }
#     last_name               { Faker::Name.last_name }
#     email                   { Faker::Internet.email }
#     password                "123456789"
#     password_confirmation   "123456789"
#   end
# end
  factory :user do 
    first_name              { Faker::Name.first_name }
    last_name               { Faker::Name.last_name }
    email                   { Faker::Internet.email }
    password                "123456789"
    password_confirmation   "123456789"

    # user_with_posts will create post data after the user has been created
    factory :user_with_tasks do
      # posts_count is declared as an ignored attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      # ignore do
      #   post { create :post }# tasks_count 5
      # end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including ignored
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the post
      after :create do |user|
        # create_list(:task, evaluator.tasks_count, user: user)
        user.tasks << FactoryGirl.create(:task)
        user.save
      end
    end
  end
end