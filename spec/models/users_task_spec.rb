require 'rails_helper'

describe UserTask do 
  it { should validate_uniqueness_of(:task_id).scoped_to(:user_id) }
  it { should belong_to(:user) }
  it { should belong_to(:task) }
  it { should belong_to(:user).class_name('User') }

end
