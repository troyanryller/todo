require 'rails_helper'

describe Task do 
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  it { should have_many(:user_tasks) }
  it { should have_many(:users).through(:user_tasks) }

  let(:task) { FactoryGirl.create(:task) }
  let(:user) { FactoryGirl.create(:user) }
  let(:to_user) { FactoryGirl.create(:user) }

  it 'should sharing task to user' do
    expect{task.users << user}.to change{task.users.count}.from(0).to(1)
    expect(task.share(user, to_user)).to be_a UserTask
    expect(task.shared_to(to_user)).to be_a User
  end



end
