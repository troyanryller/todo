require 'rails_helper'

describe User do
  it { should have_many(:user_tasks) }
  it { should have_many(:tasks).through(:user_tasks) } 

  let(:user) { FactoryGirl.build(:user, first_name: "John", last_name: "Snow") }

  it { expect(user.full_name).to eq("John Snow") }
end
