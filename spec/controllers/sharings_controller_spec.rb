require 'rails_helper'

RSpec.describe SharingsController, :type => :controller do
  let(:user) { FactoryGirl.create(:user_with_tasks) }
  let(:second_user) { FactoryGirl.create(:user) }

  before { sign_in user }

  describe "GET 'create'" do
    it "returns http success" do
      get 'create', sharing: { email: second_user.email, task_id: user.tasks.last.id }
      expect(response).to redirect_to root_path
    end
  end

end
