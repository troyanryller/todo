require 'rails_helper'

RSpec.describe TasksController, :type => :controller do

  let(:task) { FactoryGirl.create(:task) }
  let(:user) { FactoryGirl.create(:user_with_tasks) }
  let(:user_task) { user.tasks.last }
  let(:valid_attributes) { FactoryGirl.attributes_for(:task) }
  let(:invalid_attributes) { FactoryGirl.attributes_for(:task, title: '') }

  before { sign_in user }

  describe "GET index" do
    it "assigns user's tasks as @tasks" do
      get :index
      expect(controller.send(:resources)).to eq([user_task])
    end
  end

  describe "GET show" do
    it "assigns the requested user's task as @resource" do
      get :show, {:id => user_task.to_param}
      expect(controller.send(:resource)).to eq(user_task)
    end
  end

  describe "GET new" do
    it "assigns a new task as @resource" do
      get :new
      expect(controller.send(:resource)).to be_a_new(Task)
    end
  end

  describe "GET edit" do
    it "assigns the requested task as @resource" do
      get :edit, {:id => user_task.to_param}
      expect(controller.send(:resource)).to eq(user_task)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Task" do
        expect {
          post :create, {:task => valid_attributes}
        }.to change(Task, :count).by(1)
      end

      it "assigns a newly created task as @resource" do
        post :create, {:task => valid_attributes}
        expect(controller.send(:resource)).to be_a(Task)
        expect(controller.send(:resource)).to be_persisted
      end

      it "redirects to the created task" do
        post :create, {:task => valid_attributes}
        expect(response).to redirect_to(user.tasks.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved task as @resource" do
        post :create, {:task => invalid_attributes}
        expect(assigns(:resource)).to be_a_new(Task)
      end

      it "re-renders the 'new' template" do
        post :create, {:task => invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do

    before { user.tasks << task }

    describe "with valid params" do
      let(:new_attributes) { FactoryGirl.attributes_for(:task) }

      it "updates the requested task" do
        put :update, {:id => task.to_param, :task => new_attributes}
        task.reload
        expect(user.tasks.last.attributes.except('id','created_at','updated_at')).to eq(new_attributes.stringify_keys)
      end

      it "assigns the requested task as @resource" do
        put :update, {:id => task.to_param, :task => valid_attributes}
        expect(assigns(:resource)).to eq(task)
      end

      it "redirects to the task" do
        put :update, {:id => task.to_param, :task => valid_attributes}
        expect(response).to redirect_to(task)
      end
    end

    describe "with invalid params" do
      it "assigns the task as @resource" do
        put :update, {:id => task.to_param, :task => invalid_attributes}
        expect(assigns(:resource)).to eq(task)
      end

      it "re-renders the 'edit' template" do
        put :update, {:id => task.to_param, :task => invalid_attributes}
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do

    before { user.tasks << task }

    it "destroys the requested task" do
      expect {
        delete :destroy, {:id => task.to_param}
      }.to change(Task, :count).by(-1)
    end

    it "redirects to the tasks list" do
      delete :destroy, {:id => task.to_param}
      expect(response).to redirect_to(tasks_url)
    end
  end

end
