class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :user_tasks
  has_many :tasks, :through => :user_tasks 
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def full_name
    [first_name, last_name].join(' ')
  end

  # def share(task, to_user)
  #   UserTask.create user_id: to_user.id, task_id: task.id, shared_by_id: self.id
  # end

end
