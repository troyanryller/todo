class Task < ActiveRecord::Base
  has_many :user_tasks
  has_many :users, :through => :user_tasks 

  validates_presence_of [:title, :description] 

  def shared_to(user)
    shared_by = user.user_tasks.find_by(task_id: id).andand.shared_by_id
    User.find_by_id(shared_by)
  end

  def share(from_user, to_user)
    UserTask.create user_id: to_user.id, task_id: id, shared_by_id: from_user.id
  end

end
