class UserTask < ActiveRecord::Base
  belongs_to :user
  belongs_to :task
  belongs_to :shared_by, class: 'User'

  validates_uniqueness_of :task_id, :scope => :user_id

end
