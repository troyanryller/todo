window.updateTable = ->
  $.get "index", (data) ->
    $("#tasks-list").html(data)


$(document).on 'click', '.task-check', ->
  $.ajax
    type: "POST"
    url: "/tasks/ #{this.value}"
    data: _method:'PUT', task: { done : this.checked }
    dataType: 'json'
