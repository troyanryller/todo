class TasksController < ApplicationController
  before_action :authenticate_user!

  def index
    if request.xhr?
      render :partial => "table"
    end
  end

  def show
  end

  def new
    @resource = Task.new
  end

  def edit
  end

  def create
    @resource = Task.new(task_params)
    resource.users << current_user
    resource.save ? redirect_to(resource, notice: 'Task was successfully created.') : render(:new)
  end

  def update
    respond_to do |format|
      format.html do 
        if resource.update(task_params) 
          redirect_to(resource, notice: 'Task was successfully updated.') 
        else
          render(:edit)
        end
      end
      format.json { render json: { success: resource.update(task_params) } }
    end
  end

  def destroy
    resource.destroy
    redirect_to tasks_url, notice: 'Task was successfully destroyed.' 
  end

  private
    def resource
      @resource ||= current_user.tasks.find(params[:id])
    end
    helper_method :resource

    def resources
      @resources ||= current_user.tasks
    end
    helper_method :resources

    def task_params
      params.require(:task).permit(:title, :description, :done)
    end
end
