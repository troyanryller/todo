class SharingsController < ApplicationController
  before_action :authenticate_user!

  def create
    if to_user = User.find_by_email(sharing_params[:email])
      task.share current_user, to_user
      flash[:alert] = "Task was successfully shared to #{to_user.full_name}"
      redirect_to root_path
    else
      flash[:alert] = "User with such email not exist"
      redirect_to root_path
    end
  end

  private
    def sharing_params
      params.require(:sharing).permit(:email, :task_id) if params[:sharing]
    end 

    def task
      @task ||= current_user.tasks.find(sharing_params[:task_id])
    end
    helper_method :task
end
